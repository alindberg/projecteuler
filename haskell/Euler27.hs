{- Find the product of the coefficients: -1000 <= a,b <= 1000 
 for the quadratic expression that produces the
 maximum number of primes for consecutive values of n >= 0.
-}
prod :: Int
prod = let ls = map f (tups (-1000) 1000)
           ((c1, c2), pr) = maximum' ls
       in c1 * c2

f :: (Int, Int) -> ((Int, Int), Int)
f (a, b) = ((a, b), pr a b)

--what about fold..
maximum' :: [((Int, Int), Int)] -> ((Int, Int), Int)
maximum' xs = maximum'' xs ((0,0), 0)
    where 
        maximum'' xs (c, m) = case xs of
            ((c', m'):xs') -> maximum'' xs' (if m' > m then (c', m') else (c, m))
            [] -> (c, m)
         
         
-- How many primes produced for (a, b):
pr :: Int -> Int -> Int
pr a b = (length . takeWhile isPrime) [quadr n a b | n <- [0..]]
          
-- e.g. (1, 41) => 40 primes, (-79, 1601) => 80 primes
quadr :: Int -> Int -> Int -> Int
quadr n a b = n^2 + a * n + b
         
tups :: Int -> Int -> [(Int, Int)]
tups l h = cart [l..h] [l..h]

cart :: [a] -> [b] -> [(a, b)]
cart xs ys = [(a, b) | a <- xs, b <- ys]

isPrime :: Int -> Bool
isPrime k | k < 2 = False
          | otherwise = null [x | x <- [2..isqrt k], k `mod` x == 0]

isqrt :: Int -> Int
isqrt = floor . sqrt . fromIntegral

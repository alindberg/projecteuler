{-
	2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
	What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
-}

divisibleByAll :: Integer -> Integer
divisibleByAll x
	| x < 1 = 0
	| otherwise = head [y | y <- [1..], sum (map (mod y) [1..x]) == 0]
import Data.Char

s = sum (toIntArray (show (2^1000)))

toIntArray :: String -> [Int]
toIntArray [] = []
toIntArray (x:xs) = (digitToInt x) : (toIntArray xs)
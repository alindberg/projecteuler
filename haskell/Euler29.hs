import qualified Data.Set as Set
{-
    All combinations a^b for 
    2 <= a <= 5
    2 <= b <= 5
    
    2^2, 2^3, 2^4, 2^5  [4, 8, 16, 32]
    3^2, 3^3, 3^4, 3^5  [9, 27, 81, 243]
    4^2, 4^3, 4^4, 4^5  [16, 64, 256, 1024]
    5^2, 5^3, 5^4, 5^5  [25, 125, 625, 3125]

    4, 8, 9, 16, 25, 27, 32, 64, 81, 125, 243, 256, 625, 1024, 3125

-}

-- (\(x, y) -> x^y)
calculate :: Int
calculate = let cs = cart [2..100] [2..100]
                xs = map (\(x, y) -> x^y) cs
                ss = quicksort xs
                dd = (Set.toList . Set.fromList) ss
            in length dd



cart :: [a] -> [b] -> [(a, b)]
cart xs ys = [(a, b) | a <- xs, b <- ys]

quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = 
    let smaller = quicksort [a | a <- xs, a <= x]
        bigger = quicksort [a | a <- xs, a > x]
    in smaller ++ [x] ++ bigger
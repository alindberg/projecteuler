--Sum of all even valued terms of fibbonacci in the range 1, 4 mill.
fib = 1 : 1 : [a + b | (a, b) <- zip fib (tail fib)]

answer = sum [x | x <- takeWhile (<= 4 * 10^6) fib, x `mod` 2 == 0]
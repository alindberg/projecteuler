--Sum of all multiples of 3 and 5 in the list of natural numbers below 1000.
answer = sum [x | x <- [1..999], x `mod` 3 == 0 || x `mod` 5 == 0]


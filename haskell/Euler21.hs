amicableSum :: Int
amicableSum = sum . map sum' $ amicable 9999

amicable :: Int -> [(Int, Int)]
amicable x = 
    let nums = [1 .. x]
        sums = (map sum . map divisors) nums
    in amc $ filter (\(a, b) -> a /= b) (zip nums sums)

--write with foldr
amc :: [(Int, Int)] -> [(Int, Int)]
amc [] = []
amc (x : xs) = 
    case filter (equal x) xs of
        [] -> amc xs
        (y:ys) -> y : amc xs
  

divisors :: Int -> [Int]
divisors y = filter (\x -> y `mod` x == 0) [1 .. y - 1]

equal :: (Int, Int) -> (Int, Int) -> Bool
equal (x, y) (z, w) = x == w && y == z

sum' :: (Int, Int) -> Int
sum' (x, y) = x + y
import Data.List.Split

fact :: Integer -> [String]
fact x = (tail . splitOn "" . show . product) [1..x]

sum' :: Integer
sum' = sum [(read :: String -> Integer) x | x <- fact 100]
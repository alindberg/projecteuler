
index :: Integer
index = fst $ head $ dropWhile (\(y, x) -> length (show x) < 1000) (zip [1..] fibs)


fibs = 0 : 1 : zipWith (+) fibs (tail fibs)

fibbonacci :: Integer -> Integer
fibbonacci 0 = 1
fibbonacci 1 = 1
fibbonacci n = fibbonacci (n - 1) + fibbonacci (n - 2)

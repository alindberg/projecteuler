{-
	A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
	Find the largest palindrome made from the product of two 3-digit numbers.
-}
maxPal = maximum palindromeProducts
palindromeProducts = [a | a <- products, palindrome a]

products = [x * y | x <- [100..999], y <- [x + 1 .. 999]]

palindrome :: (Show a) => a -> Bool
palindrome s = show s == reverse (show s)
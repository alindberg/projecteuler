
answer :: String
answer = (reverse . take 10 . reverse . show) (series 1000)

series :: Integer -> Integer
series x = sum [a^a | a <- [1 .. x]]
{- 
    Problem: Find max prime factor of number, i.e 13195
    
    Solution:
        Find truncate $ sqrt 13195 = 114.
        p is a factor if:  [p | p <- [2, 114], isPrime p, 13195 mod p == 0]
        Return max value in the list.
        
    answer 6857
-}

factors :: Integer -> [Integer]
factors x = [y | y <- [1 .. x], x `mod` y == 0]

isPrime :: Integer -> Bool
isPrime x = factors x == [1, x]
    
primes :: Integer -> [Integer]
primes x = [p | p <- [2 .. x], isPrime p]


primeFactorization :: Integer -> [Integer]
primeFactorization x 
    | x < 2 = []
    | isPrime x = [x]
    | otherwise = y : primeFactorization (x `div` y)
        where y = head [z | z <- primes (truncate $ sqrt $ (fromIntegral x)), x `mod` z == 0]
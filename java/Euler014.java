package euler;

public class Euler014 implements Euler {

	private int max, startingNumber;

	@Override
	public void compute() {
		final int MAX_STARTING_NUMBER = 1000000;

		int[] history = new int[MAX_STARTING_NUMBER];


		for (int i = 2; i < MAX_STARTING_NUMBER; i++) {

			long z = i;
			int terms = 1;

			while (z != 1) {

				z = collatz(z);

				terms++;

				if (z > 1 && z < i) {
					terms += history[(int) z];
					break;
				}

			}

			history[i] = terms;
			if (terms > max) {

				max = terms;
				startingNumber = i;
			}
		}

	}

	@Override
	public void showResult() {
		System.out.println("max = " + max);
		System.out.println("Starting number with highest amount of terms = " + startingNumber);
	}
	
	private static long collatz(long n) {
		return (n % 2 == 0) ? (n / 2) : (3 * n + 1);
	}
}

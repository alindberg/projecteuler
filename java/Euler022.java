package euler;

import java.util.Arrays;

import io.CustomFileReader;

public class Euler022 implements Euler {

	private final String alph = "abcdefghijklmnopqrstuvwxyz";
	private String[] names;
	private long sum;

	public Euler022() {
		String content = CustomFileReader.readFile("names.txt");
		names = content.replaceAll("\"", "").split(",");
	}


	@Override
	public void compute() {
		Arrays.sort(names);

		for (int i = 0; i < names.length; i++) {
			int s = 0;
			String name = names[i].toLowerCase();
			for (int j = 0; j < name.length(); j++) {
				s += alph.indexOf(name.charAt(j)) + 1;
			}
			s *= (i + 1);
			sum += s;
		}
	}

	@Override
	public void showResult() {
		System.out.println("sum is " + sum);
	}

}

package euler;

import util.MathUtil;

/*
 * replace all with NCR (40, 20)
 * 
 * */
public class Euler015 implements Euler {

	private long ans;

	@Override
	public void compute() {
		ans = MathUtil.choose(40, 20);
	}

	@Override
	public void showResult() {
		System.out.println(ans);
	}

}

package euler;

import util.MathUtil;

public class Euler010 implements Euler {

	private long sum;

	@Override
	public void compute() {
		sum = 0;
		for (int i = 0; i < 2_000_000; i++) {
			if (MathUtil.isPrime(i)) {
				sum += i;
			}
		}
	}

	@Override
	public void showResult() {
		System.out.println("sum = " + sum);
	}

}

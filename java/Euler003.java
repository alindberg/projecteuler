package euler;

/**
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * 
 * What is the largest prime factor of the number 600851475143 ?
 */

public class Euler003 implements Euler {

	private long b;
	private final long n = 6008_5147_5143L;

	@Override
	public void compute() {
		b = n;
		int i = 0;
		while (i < b) {
			i = 2;
			for (; i < b; i++) {
				if (b % i == 0) {
					b = b / i;
				}
			}
		}
	}

	@Override
	public void showResult() {
		System.out.println("Greatest common factor of\n" + n + " is " + b);
	}

}
package euler;

public class Euler009 implements Euler {
	
	private int a, b, c;

	@Override
	public void compute() {
		int roof = 500; // this is terrible but whatever

		a = 1;
		boolean found = false;
		while (!found) {

			boolean B = true;
			b = a + 1;
			while (B && b < roof) {

				boolean C = true;
				c = b + 1;

				while (C && c < roof) {
					int abSquared = (int) Math.pow(a, 2) + (int) Math.pow(b, 2);
					int cSquared = (int) Math.pow(c, 2);
					if (abSquared == cSquared && a + b + c == 1000) {
						found = true;
					}
					c++;
				}

				b++;
			}
			a++;

		}
	}

	@Override
	public void showResult() {
		System.out.println("a = " + a + ", b = " + b + ", c = " + c);
		System.out.println("Product abc = " + (a * b * c));
	}

}

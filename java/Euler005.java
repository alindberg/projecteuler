package euler;

/**
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder. 
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*/

public class Euler005 implements Euler {

	private int x;

	@Override
	public void compute() {
		x = 20;
		while (true) {
			boolean success = true;
			for (int i = 1; i <= 20; i++) {
				if (!(x % i == 0)) {
					success = false;
					break;
				}
			}
			if (success) {
				break;
			} else {
				x += 2;
			}

		}
	}

	@Override
	public void showResult() {
		System.out.println("Number is: " + x);
	}

}
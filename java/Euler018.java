package euler;

import java.util.List;

import io.CustomFileReader;

public class Euler018 implements Euler {
	
	private List<List<Integer>> arrays;
	
	public Euler018() {
		arrays = CustomFileReader.readNumbers("Triangle1.txt");
	}


	@Override
	public void compute() {
		for (int i = arrays.size() - 2; i >= 0; i--) {
			for (int j = 0; j < arrays.get(i).size(); j++) {
				int curr = arrays.get(i).get(j);
				int max = Math.max(arrays.get(i + 1).get(j), arrays.get(i + 1).get(j + 1));
				arrays.get(i).set(j, curr + max);
			}
		}
	}

	@Override
	public void showResult() {
		System.out.println("Sum is: " + arrays.get(0).get(0));
	}

	private void showArrays() {
		for (List<Integer> k : arrays) {
			for (Integer z : k) {
				System.out.print(z + " ");
			}
			System.out.println();
		}
	}

}

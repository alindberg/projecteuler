package euler;

import util.MathUtil;

public class Euler007 implements Euler {
	
	private int[] primes;


	@Override
	public void compute() {
		primes = new int[10001];
		int counter = 0;
		int i = 1;
		while (counter < primes.length) {
			if (MathUtil.isPrime(i)) {
				primes[counter++] = i;
				// System.out.print(i + " ");
				// if(counter%10 == 0) System.out.println();
			}
			i++;

		}
	}

	@Override
	public void showResult() {
		System.out.println("10001's prime: " + primes[10000]);
	}

}

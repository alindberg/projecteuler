package euler;

/**
 * A palindromic number reads the same both ways. The largest palindrome made
 * from the product of two 2-digit numbers is 9009 = 91 × 99.
 * 
 * Find the largest palindrome made from the product of two 3-digit numbers.
 * 
 */

public class Euler004 implements Euler {

	private int biggest;
	private int[] numbers;

	@Override
	public void compute() {
		numbers = new int[2];
		for (int i = 999; i >= 500; i--) {
			for (int j = 999; j >= 500; j--) {
				String num = String.valueOf(i * j);
				String rev = "";
				for (int m = num.length() - 1; m >= 0; m--) {
					rev += num.charAt(m);
				}
				if (num.equals(rev) && (i * j > biggest)) {
					biggest = i * j;
					numbers[0] = i;
					numbers[1] = j;
				}
			}
		}
	}

	@Override
	public void showResult() {
		System.out.println("The largest palindromic number is " + biggest);
		System.out.println("Made out of: " + numbers[0] + ", and " + numbers[1]);
	}

}

package euler;

/**
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we
 * get 3, 5, 6 and 9.
 * 
 * The sum of these multiples is 23. Find the sum of all the multiples of 3 or 5
 * below 1000.
 */

public class Euler001 implements Euler {

	private int sum;

	public void compute() {

		for (int i = 1000 - 1; i > 1; i--) {
			if (i % 3 == 0 || i % 5 == 0) {
				sum += i;
			}
		}
	}

	@Override
	public void showResult() {
		System.out.println("Sum of all multiples of 3 or 5 below 1000: " + sum);
	}

}
package euler;

public interface Euler {

	public void compute();

	public void showResult();
}

package euler;

import util.Timer;

public class EulerRunner {

	
	public static void main(String[] args) {
		
		//Choose euler to run
		Euler euler = new Euler003();
		
		//Do the computation
		Timer.start();
		euler.compute();
		Timer.stop();
		
		euler.showResult();
		System.out.println("Computed in " + Timer.getSecondsElapsed() + " seconds");
		

	}
	

}

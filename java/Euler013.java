package euler;

import java.math.BigInteger;
import java.util.List;

import io.CustomFileReader;

public class Euler013 implements Euler {
	
	private List<String> numbers;
	private String filename = "bignumber.txt";
	private BigInteger sum;
	
	public Euler013() {
		numbers = CustomFileReader.readFileByLine(filename);
	}

	@Override
	public void compute() {
		BigInteger sum = new BigInteger("0");
		for (String k : numbers) {
			sum = sum.add(new BigInteger(k));
		}
	}

	@Override
	public void showResult() {
		System.out.println("first 10 digits of sum: " + sum.toString().substring(0, 10));
	}

}

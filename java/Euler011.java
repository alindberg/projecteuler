package euler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/*
	What is the greatest product of four adjacent numbers
	in the same direction (up, down, left, right, or diagonally) in the 20×20 grid?
*/
public class Euler011 implements Euler {

	private final int WIDTH = 20, HEIGHT = 20;
	private final int[][] table;

	private long highest;

	public Euler011() {
		table = new int[HEIGHT][WIDTH];

		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(new File("grid11.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		for (int y = 0; y < HEIGHT; y++) {
			String s = fileScanner.nextLine();
			Scanner lineScanner = new Scanner(s);

			for (int x = 0; x < WIDTH; x++) {
				int z = lineScanner.nextInt();
				table[y][x] = z;
			}
		}
	}
	

	@Override
	public void compute() {

		int v = 4; // how many adjacents

		for (int y = 0; y < HEIGHT; y++) {
			for (int x = 0; x < WIDTH; x++) {

				boolean posxInside = (x + v) < WIDTH;
				boolean negxInside = (x - v) > -1;

				boolean posyInside = (y + v) < HEIGHT;
				boolean negyInside = (y - v) > -1;

				int sum = 1;
				// check in positive x direction.
				if (posxInside) {

					for (int a = x; a < (x + v); a++) {
						sum *= table[y][a];
					}

					if (sum > highest) {
						highest = sum;
					}
				}

				// check in positive y direction.
				sum = 1;
				if (posyInside) {

					for (int b = y; b < (y + v); b++) {
						sum *= table[b][x];
					}

					if (sum > highest) {
						highest = sum;
					}
				}

				// check diagonal in positive y and x direction.
				sum = 1;
				if (posxInside && posyInside) {
					int q = x;
					for (int z = y; z < (y + v); z++) {
						sum *= table[z][q++];
					}

					if (sum > highest) {
						highest = sum;
					}
				}

				// check diagonal in positive y and negative x direction.
				sum = 1;
				if (posyInside && negxInside) {
					int p = x;
					for (int w = y; w < (y + v); w++) {
						sum *= table[w][p--];
					}

					if (sum > highest) {
						highest = sum;
					}
				}

			}
		}

	}

	@Override
	public void showResult() {
		System.out.println("Highest product: " + highest);
	}

}
